malis — Modular Arch Linux Installation Script
==============================================================================

Warning: Third-party installers like this one are not officially supported by
Arch Linux!

The malis script uses mbs (Modular Bash Scripts, see [1]) to split the
installation steps (e.g. filesystem creation, bootstrapping, configuration…)
into separate modules. Modules to be used during installation and their
configuration are set in the configuration file, some (but not all) options
may be set (or overridden) using command line parameters.

Note: mbs is included in this repository as a Git submodule. Use the
--recurse-submodules option when cloning.

Usage:

   ./malis [<config>] [<mbs options>] -- [<module options>]

The <config> parameter is required in most cases. It must be the basename of
a configuration file in the configs directory, without the .conf extension.

See configs/common.conf and configs/example.conf for example configurations.

Documentation of individual modules can be found in the modules' scripts, see
the modules directory. To get a list of supported command line options for the
selected modules, use

   ./malis <config> -h

License
------------------------------------------------------------------------------

Copyright © 2019-2020 Stefan Göbel < malis ʇɐ subtype ˙ de >.

malis is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

malis is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
malis. If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------------

[1] https://gitlab.com/goeb/modular-bash-scripts