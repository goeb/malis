## PASSWORDS ######################################################################################################
#
# Copyright © 2019-2020 Stefan Göbel < malis ʇɐ subtype ˙ de >.
#
# This file is part of malis.
#
# malis is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# malis is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with malis. If not, see
# <https://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------------------------------------------
#
# This module may be used by other modules to ask for passwords.
#
# Usage:
#
#     * passwords_reg <name> <description>   -  Tell this module to ask for a password using the <description>.
#     * pass=$( passwords_get <name> )       -  Get the password for the specified <name>.
#
# Passwords will be gathered in the post_prepare stage, modules must call passwords_reg before that (e.g. in the
# prepare stage). Note: passwords_get will print only the password as entered without a terminating newline!
# The module will only ask once for every password by default, to ask twice, the $cfg_verifypw option and the
# --verify-passwords command line option may be used.

passwords_main() {

   case "$1" in
      depends     ) passwords_depends      ;;
      options     ) passwords_options      ;;
      setopts     ) passwords_setopts      ;;
      post_prepare) passwords_post_prepare ;;
   esac

}

passwords_depends() {

   printf 'config\n'

}

passwords_options() {

   printf 'verify-passwords   Ask twice for passwords.\n'

}

passwords_setopts() {

   if mbs_opt verify-passwords ; then
      declare -gi cfg_verifypw='1'
   fi

}

passwords_post_prepare() {

   declare -gA passwords_pwds

   local _key=''
   for _key in "${!passwords_desc[@]}" ; do

      while true ; do

         local _pass=''

         read -p "${passwords_desc[$_key]}: " -r -s _pass
         printf '\n'

         passwords_pwds["$_key"]=$_pass

         if (( ${cfg_verifypw:-0} )) ; then

            read -p "${passwords_desc[$_key]} (verify): " -r -s _pass
            printf '\n'

            if [[ "${passwords_pwds[$_key]}" != "$_pass" ]] ; then
               printf 'Passwords do not match!\n'
               continue
            fi

         fi

         break

      done

   done

}

passwords_get() {

   printf '%s' "${passwords_pwds[$1]}"

}

passwords_reg() {

   local _key=$1
   local _dsc=$2

   declare -gA passwords_desc
   passwords_desc["$_key"]=$_dsc

}

mbs_readonly_f passwords

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115:##############################################
