## MKFS ###########################################################################################################
#
# Copyright © 2019-2020 Stefan Göbel < malis ʇɐ subtype ˙ de >.
#
# This file is part of malis.
#
# malis is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# malis is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with malis. If not, see
# <https://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------------------------------------------
#
# Create filesystems during the filesystem stage. Supported types are Ext4 and Btrfs. Configuration is done via
# config file only, using the $cfg_fsystems array. Elements are strings, one per filesystem, in the following
# format:
#
#     "<device> [; …] | <fs type> [; <subvolume>] | <mount point> | <mkfs option> [; …] | <mount options>"
#
# The <device> must be specified relative to /dev, e.g. "sda1" instead of "/dev/sda1". Supported filesystem types
# are "ext4" and "btrfs". For Btrfs, multiple devices may be specified, separated by ";".
#
# If a <subvolume> is specified for the filesystem it will be created (only for Btrfs; the filesystem itself will
# also be created if it does not exist), all actions will affect this subvolume. To create more than one subvolume
# on one device the device entry in the array may be repeated as many times as required. Don't include the "subvol"
# option in the mount options, it will be added automatically! Parent directories will be created as required.
#
# The <mount point> option may be one of the following:
#
#     * /<directory>    - Mount it at <directory> in the new system. Must start with a slash!
#     * @<name>         - Create the filesystem and mount it at the (temporary) directory <name>.
#     * :NONE           - Create the filesystem, but do not mount it.
#
# These options may be combined or repeated, separated by semicolon, the order they are specified in does not
# matter. The @<name> option will use the directories module to request the temporary directory by that <name>, so
# it may be configured as any other using the --dir <name>=<path> option if required.
#
# The mkfs.* options must be separated by ";". The mount options must be a single string separated by "," (as used
# in fstab).
#
# Filesystem config order matters for mount points, parent directories must be specified before subdirectories,
# same for subvolumes (only for nested subvolumes, subvolume parent directories will be created as required)!

mkfs_main() {

   case "$1" in
      depends   ) mkfs_depends    ;;
      options   ) mkfs_options    ;;
      setopts   ) mkfs_setopts    ;;
      filesystem) mkfs_filesystem ;;
   esac

}

mkfs_depends() {

   printf 'baseinstall,config,debug,directories\n'

}

mkfs_options() {

   printf 'mkfs-force Force filesystem creation.\n'

}

mkfs_setopts() {

   if mbs_opt mkfs-force ; then
      cfg_fs_force='1'
   fi

}

mkfs_filesystem() {

   local _root=$( directories_get root )

   declare -ga mkfs_umount_dirs=()
   mbs_register_cleanup_handler mkfs_umount
   mbs_register_error_handler   mkfs_umount

   local _cnf=''
   for _cnf in "${cfg_fsystems[@]}" ; do

      local _dev='' _typ='' _mnt='' _cop='' _mop='' _vol=''

      IFS='|' read    _dev _typ _mnt _cop _mop <<<"$( trimcfg "$_cnf" )"
      IFS=';' read    _typ _vol                <<<"$_typ"
      IFS=';' read -a _dev                     <<<"$_dev"
      IFS=';' read -a _cop                     <<<"$_cop"

      if (( ${cfg_fs_force:-0} )) ; then
         case "$_typ" in
            btrfs) _cop+=( '-f' ) ;;
            ext4 ) _cop+=( '-F' ) ;;
         esac
      fi

      # Add the filesystem module to the initramfs. Should at least work for ext4 and btrfs.

      cfg_mk_kmods+=( "$_typ" )

      # Add the packages to be installed:

      case "$_typ" in
         btrfs) cfg_add_pkgs+=( 'btrfs-progs' ) ;;
         ext4 ) cfg_add_pkgs+=( 'e2fsprogs'   ) ;;
      esac

      # BTRFS with a subvolume:

      if [[ "$_typ" == 'btrfs' && -n "$_vol" ]] ; then

         # Get the mount point of the top level volume if it exists:

         local _tvl=''
         _tvl=$(
            :: findmnt --noheadings --output TARGET --raw --source "/dev/${_dev[0]}" --options 'subvol=/' || :
         )

         # If not, create the filesystem and mount the top level volume. Whether an existing FS will be overwritten
         # depends on the $cfg_fs_force option, but checking is only performed by the mkfs.* program.

         if [[ -z "$_tvl" ]] ; then
            :: mkfs.btrfs "${_cop[@]}" "${_dev[@]/#//dev/}"
            directories_reg
            _tvl=$( directories_get )
            :: mount -t btrfs -o "$_mop${_mop:+,}subvol=/" "/dev/${_dev[0]}" "$_tvl"
            mkfs_umount_dirs+=( "$_tvl" )
         fi

         # Create all intermediate directories and the subvolume:

         :: mkdir -p "$_tvl/$( dirname "$_vol" )"
         :: btrfs subvolume create "$_tvl/$_vol"

         # Add the 'subvol' mount option:

         _mop+="${_mop:+,}subvol=/$_vol"

      # Simple filesystem creation:

      else

         :: "mkfs.$_typ" "${_cop[@]}" "${_dev[@]/#//dev/}"

      fi

      # Mount the filesystem.

      IFS=';' read -a _mnt <<< "$_mnt"

      local _arg=()
      if [[ -n "$_mop" ]] ; then
         _arg+=( -o "$_mop" )
      fi

      local _dir=''
      for _dir in "${_mnt[@]}" ; do

         if [[ -z "$_dir" || "$_dir" == ':NONE' ]] ; then
            continue
         fi

         case "$_dir" in
            /*)
               :: mkdir -p "$_root/$_dir"
               :: mount -t "$_typ" "${_arg[@]}" "/dev/${_dev[0]}" "$_root/$_dir"
               mkfs_umount_dirs+=( "$_root/$_dir" )
               ;;
            @*)
               directories_reg "${_dir:1}"
               local _tmp=$( directories_get "${_dir:1}" )
               :: mount -t "$_typ" "${_arg[@]}" "/dev/${_dev[0]}" "$_tmp"
               mkfs_umount_dirs+=( "$_tmp" )
               ;;
         esac

      done

   done

}

mkfs_umount() {

   local _ids=( "${!mkfs_umount_dirs[@]}" )
   local _idx=''

   for (( _idx=${#_ids[@]} - 1 ; _idx >= 0 ; _idx -- )) ; do
      :: umount "${mkfs_umount_dirs[_ids[_idx]]}"
   done

}

mbs_readonly_f mkfs

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115:##############################################
