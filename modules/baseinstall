## BASEINSTALL ####################################################################################################
#
# Copyright © 2019-2020 Stefan Göbel < malis ʇɐ subtype ˙ de >.
#
# This file is part of malis.
#
# malis is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# malis is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with malis. If not, see
# <https://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------------------------------------------
#
# Runs pacstrap and does some basic configuration. (The path of the temporary pacman config file used with pacstrap
# will be available in the global variable $baseinstall_tmpcfg after this module's pre_install stage has been run.)
#
# This module also provides the :/ function to run commands with chroot in the new system (basically just a wrapper
# calling arch-chroot with the correct root directory). Usage: :/ <command> [<command options>].
#
# Note: The :/ function does not check for debug mode, i.e. to not run it in debug mode, use: `:: :/ …`.
#
# Note: This modules does not install any kernel or firmware. These have to be added manually if required!

baseinstall_main() {

   case "$1" in
      depends    ) baseinstall_depends     ;;
      options    ) baseinstall_options     ;;
      setopts    ) baseinstall_setopts     ;;
      prepare    ) baseinstall_prepare     ;;
      pre_install) baseinstall_pre_install ;;
      install    ) baseinstall_install     ;;
   esac

}

baseinstall_depends() {

   printf 'config,debug,directories\n'

}

baseinstall_options() {

   printf 'add-pkg<package>@  Additional package to install (may be repeated).\n'
   printf 'del-pkg<package>@  Package to exclude from installation (may be repeated).\n'
   printf 'mirror<regex>      Regular expression to select the pacman mirror(s).\n'
   printf 'pacman-conf<file>  pacman config file. Empty string to use the default.\n'

}

baseinstall_setopts() {

   if mbs_opt mirror      ; then cfg_mirrorrx=$( mbs_val mirror      ) ; fi
   if mbs_opt pacman-conf ; then cfg_pac_conf=$( mbs_val pacman-conf ) ; fi

   if mbs_opt add-pkg ; then
      mbs_lst add-pkg _baseinstall_add
      cfg_add_pkgs+=( "${_baseinstall_add[@]}" )
      unset _baseinstall_add
   fi

   if mbs_opt del-pkg ; then
      mbs_lst del-pkg _baseinstall_del
      cfg_xcl_pkgs+=( "${_baseinstall_del[@]}" )
      unset _baseinstall_del
   fi

   if [[ -z "${cfg_pac_conf:-}" ]] ; then
      cfg_pac_conf='/etc/pacman.conf'
   fi

}

baseinstall_prepare() {

   directories_reg base
   directories_reg root

}

baseinstall_pre_install() {

   local _base=''
   local _root=''
   local _tmpl='malis.XXXXXXXX'

   _base=$( directories_get base )
   _root=$( directories_get root )

   # Modify the mirror list if $cfg_mirrorrx is set and we're in the live (installation ISO) environment. On a real
   # system we assume everything is configured correctly.

   if [[ -n "${cfg_mirrorrx:-}" ]] && :: -r 0 install_iso ; then

      local _lst=''
      _lst=$( :: -p "$( mktemp -u -p "$_base" "$_tmpl" )" mktemp -p "$_base" "$_tmpl" )
      :: -w "$_lst" grep -E "$cfg_mirrorrx" /etc/pacman.d/mirrorlist

      :: mv -f "$_lst" /etc/pacman.d/mirrorlist
      :: chmod 0644    /etc/pacman.d/mirrorlist

   fi

   # Since pacstrap is just a dumb wrapper around a dumb pacman -r …, we need to deal with any hooks in
   # /etc/pacman.d/hooks/. Easiest way seems to be to remove all HookDir directives from the config, and add
   # another [options] section at the end to override the default.

   baseinstall_tmpcfg=$( :: -p "$( mktemp -u -p "$_base" "$_tmpl" )" mktemp -p "$_base" "$_tmpl" )
   readonly baseinstall_tmpcfg

   mbs_register_cleanup_handler baseinstall_rmconf
   mbs_register_error_handler   baseinstall_rmconf

   :: -w "$baseinstall_tmpcfg" grep -v '^[[:space:]]*HookDir' "${cfg_pac_conf:-/etc/pacman.conf}"
   :: -a "$baseinstall_tmpcfg" printf '\n[options]\nHookDir = /var/empty/\n'

   # With a small amount of RAM downloading the packages may fail. Mount a bigger tmpfs on the cache dir (in the
   # new system - host config does not seem to influence the path in the chroot). 2GiB should be enough.

   :: mkdir -p "$_root/var/cache/pacman/pkg"
   :: mount -t tmpfs -o size=2g pkgcache "$_root/var/cache/pacman/pkg"

   mbs_register_cleanup_handler baseinstall_umount
   mbs_register_error_handler   baseinstall_umount

}

baseinstall_install() {

   # Run pacstrap to install the base system and any additional packages as configured.

   local _root=''
   _root=$( directories_get root )

   :: pacman --config "$baseinstall_tmpcfg" -Sy
   :: pacstrap -C "$baseinstall_tmpcfg" -G "$_root" $( baseinstall_resolve )

}

# Cleanup/error handler to remove the temporary pacman config file.
#
baseinstall_rmconf() {

   if [[ -n "${baseinstall_tmpcfg:-}" ]] ; then
      :: rm -f "$baseinstall_tmpcfg"
   fi

}

# Cleanup/error handler to unmount the package cache tmpfs.
#
baseinstall_umount() {

   local _root=''
   _root=$( directories_get root )

   if :: findmnt --type tmpfs --source pkgcache --mountpoint "$_root/var/cache/pacman/pkg" >/dev/null 2>&1 ; then
      :: umount "$_root/var/cache/pacman/pkg"
   fi

}

# Usage: baseinstall_resolve [<packages or groups>]
#
# Creates the package list to install. Packages (and packages from groups) specified as parameters will be added,
# as well as the base group and all packages or groups in the $cfg_add_pkgs array, then all packages in the
# $cfg_xcl_pkgs will be removed. The resulting list will be printed to STDOUT (packages separated by spaces).
#
baseinstall_resolve() {

   declare -ga cfg_add_pkgs
   declare -ga cfg_xcl_pkgs

   local -a _org=()
   local -a _pkg=()
   local -a _grp=()
   local -A _xcl=()
   local    _cur=''

   if [[ -v cfg_xcl_pkgs ]] ; then
      for _cur in "${cfg_xcl_pkgs[@]}" ; do
         _xcl["$_cur"]='1'
      done
   fi

   _org=( 'base' "${cfg_add_pkgs[@]}" "$@" )

   for _cur in "${_org[@]}" ; do

      if [[ -z "$_cur" ]] ; then
         continue
      fi

      # base is now a (meta) package and a group. Check for package existence first, then try to resolve the group
      # members.

      if pacman -Si "$_cur" >/dev/null 2>&1 ; then
         _grp=( "$_cur" )
      elif ! _grp=( $( pacman -Sgq "$_cur" 2>/dev/null ) ) ; then
         printf 'Package or group %s not found.\n' "$_cur" >&2
         return 1
      fi

      for _cur in "${_grp[@]}" ; do
         if [[ -z "${_xcl[$_cur]:-}" ]] ; then
            _pkg+=( "$_cur" )
         fi
      done

   done

   printf '%s\n' "${_pkg[*]}"

}

# Usage: :/ <command> [<options>]
#
# Run the <command> (with its <options>) in the new root using arch-chroot. Note that before arch-chroot is called,
# the root directory is checked for being a mount point, and if not, a (recursive) bind mount on itself is created.
# It will be unmounted again after the arch-chroot call.
#
# (Turn POSIX mode off/on due to function name.)

set +o posix

:/() {

   local _root=''
   local _bind='0'

   _root=$( directories_get root )

   if ! findmnt --mountpoint "$_root" >/dev/null 2>&1 ; then
      mount --rbind "$_root" "$_root"
      _bind='1'
   fi

   local _result='0'
   arch-chroot "$_root" "$@" || _result=$?

   if (( _bind )) ; then
      umount "$_root"
   fi

   return "$_result"

}

set -o posix

mbs_readonly_f baseinstall

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115:##############################################
